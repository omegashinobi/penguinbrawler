﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

public class weaponPickup_DATA : MonoBehaviour {

    public void OnEnable() {
        weaponPickup_CONTROLLER = gameObject.AddComponent<weaponPickup_CONTROLLER>();
        photonView = GetComponent<PhotonView>();
    }

    public Material onMat;
    public Material offMat;

    //[HideInInspector]
    public float rechargeTimer;
    public float rechargeTimerMax;

    public bool disabled;

    public GameObject Weapon;
    public GameObject wepObject;

    public PhotonView photonView;

    [HideInInspector]
    weaponPickup_CONTROLLER weaponPickup_CONTROLLER;

}
