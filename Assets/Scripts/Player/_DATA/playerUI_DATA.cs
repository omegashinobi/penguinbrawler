﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerUI_DATA : MonoBehaviour {
    public Text killCounter;

    public void OnEnable() {
        playerUI_CONTROLLER = gameObject.AddComponent<playerUI_CONTROLLER>();

        player_DATA = gameObject.GetComponent<player_DATA>();
    }

    [HideInInspector]
    public playerUI_CONTROLLER playerUI_CONTROLLER;
    [HideInInspector]
    public player_DATA player_DATA;
}
