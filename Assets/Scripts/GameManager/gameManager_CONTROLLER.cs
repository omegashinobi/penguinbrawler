﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Photon.Pun;
using System.Collections;

public class gameManager_CONTROLLER : MonoBehaviourPun {
    gameManager_DATA data;

    private void Awake() {
        data = gameObject.GetComponent<gameManager_DATA>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update() {
        parseInput();

        if(data.startSync) {
            gameManager_DATA.syncTimer -= Time.deltaTime;
            if (gameManager_DATA.syncTimer <= 0) {
                gameManager_DATA.synced = true;
            }
        }
        
    }

    private void parseInput() {
        bool escapKey = Input.GetKeyDown(KeyCode.End);

        if(escapKey) {
            Application.Quit();
        }
    }

    public static void SpawnPlayer(string playerGlobalID) {

        gameManager_DATA.spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint").OfType<GameObject>().ToList();
        List<spawnPoint_DATA> spawnPointData = gameManager_DATA.spawnPoints.Select(e => e.GetComponent<spawnPoint_DATA>()).ToList();
        List<spawnPoint_DATA> avalible = spawnPointData.FindAll(e => e.avalible == true);

        if(avalible.Count > 0) {
            spawnPoint_DATA selected = avalible[UnityEngine.Random.Range(0, avalible.Count)];
            selected.spawnPoint_CONTROLLER.spawn(playerGlobalID);
        } else {
            Debug.Log("All spawns should be on at the start of the match");
        }

    }


    [PunRPC]
    public static void assignKill(string globalID) {

        Debug.Log(globalID + " got a kill");

        foreach(string id in gameManager_DATA.players.Keys) {
            Debug.Log("Players:" + id);
        }

        player_DATA player = gameManager_DATA.players[globalID].GetComponent<player_DATA>();

        if (gameManager_DATA.playerScoreList.ContainsKey(player.playerGlobalID)) {
            gameManager_DATA.playerScoreList[player.playerGlobalID] += 1;
        } else {
            gameManager_DATA.playerScoreList.Add(player.playerGlobalID, 1);
        }

        if(player != null) {
            player.playerCombat_CONTROLLER.updateKillCount(gameManager_DATA.playerScoreList[player.playerGlobalID]);
        }

        Debug.Log("score:");
        foreach(KeyValuePair<string, int> entry in gameManager_DATA.playerScoreList) {
            Debug.Log(entry.Key + " : " + entry.Value);
        }

        checkScoreLimit();
    }


    public static void checkScoreLimit() {
        if (gameManager_DATA.playerScoreList.Any(e => e.Value >= gameManager_DATA.scoreLimit)) {
            KeyValuePair<string, int> entry = gameManager_DATA.playerScoreList.Single(e => e.Value == gameManager_DATA.scoreLimit);

            //The game is over

            GameObject go = gameManager_DATA.players[entry.Key];
            //do something with winner;
            waitFor(gameManager_DATA.waitforNextGame, delegate { nextGame(); });
        }
    }

    public static void nextGame() {
        GameObject[] goList = GameObject.FindGameObjectsWithTag("Player");

        int index = 1;
        foreach(GameObject go in goList) {
            Destroy(go);

            waitFor(0.1f*index, delegate { SpawnPlayer(PhotonNetwork.AuthValues.UserId); });
            index++;
        }
    }

    //magical Wait mode!
    public static IEnumerator waitFor(float waitTime, Action action) {
        yield return new WaitForSeconds(waitTime);
        action();
    }
}
