﻿using UnityEngine;
using System.Collections;

public class weapon_CONTROLLER : MonoBehaviour {

    private weapon_DATA data;
    void Awake() {
        data = gameObject.GetComponent<weapon_DATA>();
        gameObject.name = data.weaponName;

    }
    public void fire(Vector3 direction, Quaternion rotation,string playerID) {

        data.fireRateMin -= Time.deltaTime;

        if (data.fireRateMin <= 0) {
            GameObject go = Instantiate(data.bullet, data.bulletSpawn.transform.position, rotation) as GameObject;


            go.GetComponent<bullet_DATA>().damage = data.damage;
            go.GetComponent<bullet_DATA>().direction = direction;
            go.GetComponent<bullet_DATA>().rotation = rotation;
            go.GetComponent<bullet_DATA>().playerID = playerID;

            go.transform.parent = null;

            data.fireRateMin = data.fireRate;
        }
    }

}
