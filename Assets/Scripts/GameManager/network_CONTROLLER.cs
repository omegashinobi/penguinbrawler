﻿using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;
using System.Linq;
public class network_CONTROLLER : MonoBehaviourPunCallbacks {

    public gameManager_DATA data;
    public void Awake() {
        data = gameObject.GetComponent<gameManager_DATA>();

        data.startSync = false;
        gameManager_DATA.synced = false;
    }

    private void Start() {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        Debug.Log("Connected to MASTER server");
        Debug.LogFormat("Region {0}", PhotonNetwork.NetworkingClient.CloudRegion);


        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom() {
        Debug.Log("Room Joined");

        foreach (player_DATA playerData in gameManager_DATA.players.Values.Select(e => e.GetComponent<player_DATA>())) {
            playerData.playerCombat_CONTROLLER.updateState();
        }

        data.startSync = true;

        gameManager_CONTROLLER.SpawnPlayer(PhotonNetwork.AuthValues.UserId);
    }

    [PunRPC]
    public static void refreshPlayers() {
        gameManager_DATA.players.Clear();

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Player")) {
            gameManager_DATA.players.Add(go.GetComponent<player_DATA>().playerGlobalID, go);
        }
    }

    public void OnPlayerConnected() {
        data.photonView.RPC("refreshPlayers", RpcTarget.All);
    }

    public void OnPlayerDisconnected() {
        data.photonView.RPC("refreshPlayers", RpcTarget.All);
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        Debug.Log(returnCode);
        Debug.Log(message);

        Debug.Log("Attempting to Create room....");
        PhotonNetwork.CreateRoom("PenguinTestRoom", new Photon.Realtime.RoomOptions());
    }

    public override void OnCreateRoomFailed(short returnCode, string message) {
        Debug.Log("Room Creation Failed");

        Debug.Log(returnCode);
        Debug.Log(message);
    }

    public override void OnCreatedRoom() {
        Debug.Log("Room Created!");

        refreshPlayers();

        //gameManager_CONTROLLER.waitFor(2, delegate { gameManager_CONTROLLER.SpawnPlayer(PhotonNetwork.AuthValues.UserId); });

    }





}
