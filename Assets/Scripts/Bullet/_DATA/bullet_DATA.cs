﻿using UnityEngine;
using System.Collections;

public class bullet_DATA : MonoBehaviour {

    private void Awake() {
        bullet_CONTROLLER = gameObject.AddComponent<bullet_CONTROLLER>();
    }

    public float speed;
    public float dipRate;

    [HideInInspector]
    public float travel;
    public float maxTravel;

    public float cleanUpTimer;

    public Vector3 direction;
    public Quaternion rotation;

    public int damage;

    public string playerID;

    [HideInInspector]
    public bullet_CONTROLLER bullet_CONTROLLER;
}
