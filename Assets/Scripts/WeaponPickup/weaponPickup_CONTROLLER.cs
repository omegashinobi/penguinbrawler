﻿using UnityEngine;
using System.Collections.Generic;
using Photon.Pun;
using System.Linq;

public class weaponPickup_CONTROLLER : MonoBehaviour {
    private weaponPickup_DATA data;
    void Awake() {
        data = gameObject.GetComponent<weaponPickup_DATA>();
        data.rechargeTimer = data.rechargeTimerMax;
    }

    public void pickedUp() {
        data.disabled = true;
        data.rechargeTimer = data.rechargeTimerMax;

        data.wepObject.GetComponent<Renderer>().material = data.offMat;
    }

    public void Update() {
        data.rechargeTimer -= Time.deltaTime;

        if (data.rechargeTimer <= 0) {
            data.rechargeTimer = 0;
            data.disabled = false;
            data.wepObject.GetComponent<Renderer>().material = data.onMat;
        }
    }

    public void OnTriggerEnter(Collider other) {
        if (other.transform.tag == "Player") {
            if (!data.disabled) {
                data.photonView.RPC("assignWeapon", RpcTarget.AllBuffered, other.gameObject.GetComponent<player_DATA>().playerID);
            }
        }
    }


    [PunRPC]
    public void assignWeapon(string ID) {

        if (gameManager_DATA.synced) {
            GameObject other = null;
            List<GameObject> goList = GameObject.FindGameObjectsWithTag("Player").ToList();

            Debug.Log("Triggered Wep Pickup... Player ID:" + ID);

            foreach (GameObject go in goList) {
                Debug.Log("IDs:" + go.GetComponent<player_DATA>().playerID);

                if (go.GetComponent<player_DATA>().playerID == ID) {
                    other = go;
                }
            }
            if (other != null) {
                player_DATA playerData = other.GetComponent<player_DATA>();

                if (playerData.weapons.Count > 0) {
                    weapon_DATA wepPickupData = data.Weapon.GetComponent<weapon_DATA>();
                    List<weapon_DATA> playerWeapons = other.GetComponent<player_DATA>().weapons.Select(e => e.GetComponent<weapon_DATA>()).ToList();

                    foreach (weapon_DATA wep in playerWeapons) {
                        if (wep.ID != wepPickupData.ID) {
                            playerData.weapons.Add(data.Weapon);
                        }
                    }
                } else {
                    playerData.weapons.Add(data.Weapon);
                    playerData.equipedWeaponAvatarName = data.Weapon.name;
                    playerData.playerCombat_CONTROLLER.equipWeapon(data.Weapon);
                }

                pickedUp();
            }
        } else {
            StartCoroutine(gameManager_CONTROLLER.waitFor(gameManager_DATA.syncTimer, delegate { assignWeapon(ID); }));
        }
    }
}
