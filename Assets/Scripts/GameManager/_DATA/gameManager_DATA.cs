﻿using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

public class gameManager_DATA : MonoBehaviourPun {

    private void Awake() {
        photonView = GetComponent<PhotonView>();

        gameManager_CONTROLLER = gameObject.AddComponent<gameManager_CONTROLLER>();
        network_CONTROLLER = gameObject.AddComponent<network_CONTROLLER>();
    }

    [HideInInspector]
    public gameManager_CONTROLLER gameManager_CONTROLLER;
    [HideInInspector]
    public network_CONTROLLER network_CONTROLLER;

    public static List<GameObject> spawnPoints;

    public static Dictionary<string, GameObject> players = new Dictionary<string, GameObject>();

    public static float syncTimer = 2;
    public static bool synced;
    public bool startSync;

    public static int scoreLimit = 25;

    public static int waitforNextGame = 3;

    public static Dictionary<string, int> playerScoreList = new Dictionary<string, int>();

    public PhotonView photonView;

}
