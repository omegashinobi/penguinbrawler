﻿using UnityEngine;
using System.Collections;

public class spawnPoint_DATA : MonoBehaviour {

    private void Awake() {
        spawnPoint_CONTROLLER = gameObject.AddComponent<spawnPoint_CONTROLLER>();
    }

    public Material on;
    public Material off;

    public bool avalible;

    public float avalibleTimer;
    public float avalibleTimerMax;

    [HideInInspector]
    public spawnPoint_CONTROLLER spawnPoint_CONTROLLER;
}
