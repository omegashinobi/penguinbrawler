﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

public class playerCombat_CONTROLLER : MonoBehaviour {

    private player_DATA data;

    float fireButtonHeld;
    float fireButtonPrev;

    void Awake() {
        data = gameObject.GetComponent<player_DATA>();
    }

    public void Update() {
        if (data.photonView.IsMine) {
            parseInput();
            getWeaponPoint();
        }
    }

    public void getWeaponPoint() {

        Debug.DrawRay(data.camera.transform.position, data.transform.forward * 100.0f, Color.red);

        RaycastHit hit;
        if (Physics.Raycast(data.camera.transform.position, data.camera.transform.forward, out hit)) {

            if (hit.transform.gameObject.layer != 9) {
                data.shootDirection = hit.point;
                data.shootRotation = Quaternion.LookRotation(hit.normal);
            }

        }

    }

    [PunRPC]
    public void equipWeapon(GameObject weapon) {

        if (data.equipedWeapon) {
            Destroy(data.equipedWeaponAvatar);
        }


        GameObject go = Instantiate(weapon, gameObject.transform) as GameObject;

        go.transform.SetPositionAndRotation(
            data.gunLocation.transform.position,
            data.gunLocation.transform.rotation
        );

        data.equipedWeapon = weapon.GetComponent<weapon_DATA>();

        data.equipedWeaponAvatar = go;
        data.photonView.RPC("syncWeapon", RpcTarget.AllBuffered, data.equipedWeaponAvatar.name);


    }

    public void switchWeapon(int progress) {

        data.equipedIndex += progress;

        if (data.equipedIndex > data.weapons.Count) {
            data.equipedIndex = 0;
        }
        if (data.equipedIndex < 0) {
            data.equipedIndex = data.weapons.Count;
        }
        equipWeapon(data.weapons[data.equipedIndex]);
    }


    private void parseInput() {

        float fireButton = Input.GetAxis("Fire");
        float jumpButton = Input.GetAxis("Jump");

        if (data.equipedWeapon != null) {
            if (fireButton > 0) {
                data.photonView.RPC("fireWeapon", RpcTarget.All, data.playerGlobalID);
            }
        }
    }

    [PunRPC]
    public void fireWeapon(string ID) {
        data.equipedWeaponAvatar.GetComponent<weapon_CONTROLLER>().fire(data.shootDirection, data.shootRotation, ID);
    }

    [PunRPC]
    public void syncWeapon(string wepName) {
        data.equipedWeaponAvatarName = wepName;
    }

    [PunRPC]
    public void damage(int amount, string playerID) {
        if (!data.dead) {
            Debug.Log("Damaged For:" + amount);
            data.health -= amount;

            data.photonView.RPC("changeToDeadState", RpcTarget.All, playerID);
        }
    }

    [PunRPC]
    public void changeToDeadState(string playerID) {
        if(data.photonView.IsMine) {
            if (!data.dead) {
                if (data.health < 0) {
                    gameObject.GetComponent<Renderer>().material = data.deadMat;

                    data.dead = true;
                    gameManager_CONTROLLER.assignKill(playerID);
                    StartCoroutine(gameManager_CONTROLLER.waitFor(2, delegate { data.canMove = false; PhotonNetwork.Destroy(gameObject); gameManager_CONTROLLER.SpawnPlayer(PhotonNetwork.AuthValues.UserId); }));
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.tag == "Bullet") {
            bullet_DATA bulletData = collision.transform.GetComponent<bullet_DATA>();

            if (bulletData.playerID != data.playerID) {
                data.photonView.RPC("damage", RpcTarget.All, bulletData.damage, bulletData.playerID);
                Destroy(collision.transform.gameObject);
            }

        }
    }

    public void updateKillCount(int killCount) {
        data.killCount = killCount;
        data.playerUI_DATA.playerUI_CONTROLLER.updateKillCounter(data.killCount);
    }

    public void updateState() {
        //equipWeapon(Instantiate((GameObject)Resources.Load(data.equipedWeaponAvatarName)));
    }
}
