﻿using UnityEngine;
using System.Collections;

public class weapon_DATA : MonoBehaviour {

    private void Awake() {
        weapon_CONTROLLER = gameObject.AddComponent<weapon_CONTROLLER>();
    }

    public enum _fireType {
        normal,
        burst,
        automatic
    }

    public int ID;

    public int ammo;
    public int maxAmmo;
    public int clipSize;

    public float recoil;
    public float fireRate;

    [HideInInspector]
    public float fireRateMin;

    public string weaponName;

    public int damage;

    public GameObject bullet;
    public AudioClip[] fireSounds;

    public Transform bulletSpawn;

    public int inventroryIndex;

    [HideInInspector]
    public weapon_CONTROLLER weapon_CONTROLLER;
}
