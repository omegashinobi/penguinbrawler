﻿using UnityEngine;
using System.Collections;

public class playerCamera_CONTROLLER : MonoBehaviour {

    private player_DATA data;

    GameObject camera;

    Quaternion lastRotation;
    Vector2 lastPosition;

    private float arcMax = -0.2f;
    private float arcMin = 0.2f;

    private bool camreaOOB;

    private void OnEnable() {
        if (data.photonView.IsMine) {
            camera.SetActive(true);
        } else {
            camera.SetActive(false);
        }
    }

    void Awake() {
        data = gameObject.GetComponent<player_DATA>();
        camera = data.camera;
    }
    void Start() {
        attachCamera();
    }

    void Update() {
        //correctCamera();
    }
    void LateUpdate() {
        parseInput();
    }

    void parseInput() {
        float mouseVertical = Input.GetAxis("Mouse Y");

        float lookVerticalRotation = (mouseVertical * (data.lookSpeed * Time.deltaTime));

        if (camera.transform.localRotation.x < arcMax && mouseVertical > 0) {
            lookVerticalRotation = -0.01f;
        }

        if (camera.transform.localRotation.x > arcMin && mouseVertical < 0) {
            lookVerticalRotation = 0.01f;
        }

        Vector3 nextRot = new Vector3(
            (-lookVerticalRotation),
            (0),
            (0)
        );

        camera.transform.Rotate(nextRot);
    }

    void attachCamera() {
        if (data.cameraDefaultLoc) {
            data.cameraLocation = data.camreaRight.position;
        } else {
            data.cameraLocation = data.cameraLeft.position;
        }

        camera.transform.position = data.cameraLocation;
    }

}
