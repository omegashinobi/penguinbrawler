﻿using UnityEngine;
using System.Collections;

public class bullet_CONTROLLER : MonoBehaviour {
    private bullet_DATA data;
    void Awake() {
        data = gameObject.GetComponent<bullet_DATA>();
    }

    void Update() {
        transform.position = Vector3.MoveTowards(gameObject.transform.position, data.direction, data.speed * Time.deltaTime);
        
        data.travel++;

        if(data.travel > data.maxTravel) {
            Destroy(gameObject);
        }

        data.cleanUpTimer-=Time.deltaTime;

        if(data.cleanUpTimer <= 0) {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        Destroy(gameObject);
    }
}
