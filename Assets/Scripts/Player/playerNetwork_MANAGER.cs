﻿using UnityEngine;
using System.Collections;
using Photon.Pun;

public class playerNetwork_MANAGER : MonoBehaviourPun, IPunObservable {
    player_DATA data;
    public void OnEnable() {
        data = GetComponent<player_DATA>();

        if(data.photonView.IsMine) {
            data.playerID = System.Guid.NewGuid().ToString();
            data.playerGlobalID = PhotonNetwork.AuthValues.UserId;
        }

        PhotonNetwork.AddCallbackTarget(this);
    }

    public void OnDisable() {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(data.playerID);
            stream.SendNext(data.playerGlobalID);
            stream.SendNext(data.shootDirection);

            network_CONTROLLER.refreshPlayers();
        } else if (stream.IsReading) {
            data.playerID = (string)stream.ReceiveNext();
            data.playerGlobalID = (string)stream.ReceiveNext();
            data.shootDirection = (Vector3)stream.ReceiveNext();

            
        }
    }
}
