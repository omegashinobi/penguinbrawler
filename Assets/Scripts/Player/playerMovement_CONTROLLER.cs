﻿using UnityEngine;
using System.Collections;

public class playerMovement_CONTROLLER : MonoBehaviour {

    private player_DATA data;

    GameObject go;
    GameObject camera;



    void Awake() {
        data = gameObject.GetComponent<player_DATA>();

        go = gameObject;
        camera = GameObject.FindGameObjectWithTag("MainCamera");

        data.canMove = true;
    }
    void Update() {
        if (data.photonView.IsMine) {
            parseInput();
        }
    }

    private void parseInput() {

        if (data.canMove) {
            float horizontalAxis = Input.GetAxis("Horizontal");
            float verticalAxis = Input.GetAxis("Vertical");
            float jumpButton = Input.GetAxis("Jump");

            float mouseHorizontal = Input.GetAxis("Mouse X");


            Vector3 position = go.transform.position;
            Quaternion rotation = go.transform.rotation;

            Vector3 nextPos = new Vector3(
                (horizontalAxis * (data.moveSpeed * Time.deltaTime)),
                (0),
                (verticalAxis * (data.moveSpeed * Time.deltaTime))
            );

            Vector3 nextRotation = new Vector3(
                (0),
                ((mouseHorizontal * (data.lookSpeed * Time.deltaTime))),
                (0)
            );


            go.transform.Translate(nextPos);
            go.transform.Rotate(nextRotation);

            if (jumpButton > 0 && !data.jumping) {
                GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(0, data.jumpForce, 0), gameObject.transform.position);
                data.jumping = true;
            }

            //if(data.equipedWeaponAvatar != null) {
            //    data.equipedWeaponAvatar.transform.LookAt(data.shootDirection,Vector3.up);
            //}

        }
    }

    private void OnCollisionEnter(Collision collision) {
        if(data.jumping)
            data.jumping = false;
    }





}
