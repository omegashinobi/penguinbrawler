﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class player_DATA : MonoBehaviour {

    public string playerID;
    public string playerGlobalID;

    public enum _fireState {
        pressed,
        held
    }

    public void OnEnable() {
        photonView = GetComponent<PhotonView>();
        playerUI_DATA = GetComponent<playerUI_DATA>();


        playerCombat_CONTROLLER = gameObject.AddComponent<playerCombat_CONTROLLER>();
        movement_CONTROLLER = gameObject.AddComponent<playerMovement_CONTROLLER>();
        playerCamera_CONTROLLER = gameObject.AddComponent<playerCamera_CONTROLLER>();
    }

    public int health;

    public bool dead;
    public bool jumping;

    public float moveSpeed;
    public float lookSpeed;
    public float cameraRotateSpeed;

    public float jumpForce;

    public bool canMove;

    public Transform cameraLeft;
    public Transform camreaRight;

    public GameObject camera;

    [HideInInspector]
    public Vector3 cameraLocation;
    public bool cameraDefaultLoc;

    public int damageMultiplyer = 1;

    public List<GameObject> weapons;
    public weapon_DATA equipedWeapon;

    public GameObject equipedWeaponAvatar;
    public string equipedWeaponAvatarName;

    public int equipedIndex;

    public Material deadMat;

    public Transform gunLocation;

    public Vector3 shootDirection;
    public Quaternion shootRotation;

    public int killCount;

    [HideInInspector]
    public PhotonView photonView;

    [HideInInspector]
    public _fireState fireState;

    [HideInInspector]
    public playerMovement_CONTROLLER movement_CONTROLLER;
    [HideInInspector]
    public playerCamera_CONTROLLER playerCamera_CONTROLLER;
    [HideInInspector]
    public playerCombat_CONTROLLER playerCombat_CONTROLLER;
    [HideInInspector]
    public playerUI_DATA playerUI_DATA;

}
