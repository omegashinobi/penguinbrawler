﻿using UnityEngine;
using System.Collections;

public class playerUI_CONTROLLER : MonoBehaviour {


    private playerUI_DATA data;

    void Awake() {
        data = gameObject.GetComponent<playerUI_DATA>();
    }

    public void Start() {
        updateKillCounter(data.player_DATA.killCount);
    }


    public void updateKillCounter(int value) {
        data.killCounter.text = value.ToString();
    }


}
