﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

public class spawnPoint_CONTROLLER : MonoBehaviour {
    spawnPoint_DATA data;
    private void Awake() {
        data = gameObject.GetComponent<spawnPoint_DATA>();
        data.avalibleTimer = 0;
    }

    private void Update() {
        data.avalibleTimer -= Time.deltaTime;

        if(data.avalibleTimer <= 0) {
            data.avalibleTimer = 0;
            becomeAvalible();
        }
    }

    public void becomeAvalible() {
        data.avalible = true;
        gameObject.GetComponent<Renderer>().material = data.on;
    }
    public void disable() {

    }

    public void spawn(string playerGlobalID) {
        data.avalibleTimer = data.avalibleTimerMax;
        data.avalible = false;

        PhotonNetwork.Instantiate("Player",transform.position,transform.rotation);

        gameObject.GetComponent<Renderer>().material = data.off;
    }
}
